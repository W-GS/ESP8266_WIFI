
#define BLINKER_WIFI

#include <Blinker.h>
#include <ESP8266WiFi.h>


char auth[] = "9d51892f7a0d";

// 继电器控制IO口
#define Bedroom 0

#define ON LOW    // 开
#define OFF HIGH  // 关

int count = 0;
bool WIFI_Status = true;

// 新建组件对象
BlinkerButton Bedroom_Light("Bedroom");
bool Bedroom_state = false;
bool ALL_state = false;


// 总开关
void switch_callback(const String& state) {
  BLINKER_LOG("get switch state: ", state);

  if (state == BLINKER_CMD_ON) {
    ALL_state = true;
    Bedroom_state = true;
    digitalWrite(LED_BUILTIN, ON);
    digitalWrite(Bedroom, ON);

    Bedroom_Light.color("#FDB0AE");
    Bedroom_Light.text("卧室灯开");
    Bedroom_Light.print("on");
    BUILTIN_SWITCH.print("on");
  } else if (state == BLINKER_CMD_OFF) {
    ALL_state = false;
    Bedroom_state = false;
    digitalWrite(LED_BUILTIN, OFF);
    digitalWrite(Bedroom, OFF);

    Bedroom_Light.color("#3C3C3C");
    Bedroom_Light.text("卧室灯关");
    Bedroom_Light.print("off");
    BUILTIN_SWITCH.print("off");
  }
}

String summary() {
  String data = "online, switch: " + STRING_format(ALL_state ? "on" : "off");
  return data;
}

// 按下按键即会执行该函数
void Bedroom_callback(const String& state) {
  if (state == BLINKER_CMD_ON) {
    Bedroom_state = true;
    digitalWrite(Bedroom, ON);

    Bedroom_Light.color("#FDB0AE");
    Bedroom_Light.text("卧室灯开");
    Bedroom_Light.print("on");
  } else if (state == BLINKER_CMD_OFF) {
    Bedroom_state = false;
    digitalWrite(Bedroom, OFF);

    Bedroom_Light.color("#3C3C3C");
    Bedroom_Light.text("卧室灯关");
    Bedroom_Light.print("off");
  }
}


// 配网函数
void smartConfig() {
  WiFi.mode(WIFI_STA);  // 使用wifi的STA模式
  // Serial.println("\r\nWait for Smartconfig...");   // 串口打印
  WiFi.beginSmartConfig();  // 等待手机端发出的名称与密码
  // 死循环，等待获取到wifi名称和密码
  while (1) {
    // 等待过程中一秒打印一个.
    Serial.print(".");
    delay(1000);
    if (WiFi.smartConfigDone())  // 获取到之后退出等待
    {
      Serial.println("SmartConfig Success");
      // 打印获取到的wifi名称和密码
      Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
      Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
      break;
    }
  }
}

// WIFI初始化
void WIFI_Init() {
  Serial.println("\r\n正在连接");
  // 当设备没有联网的情况下，执行下面的操作
  while (WiFi.status() != WL_CONNECTED) {
    if (WIFI_Status)  // WIFI_Status为真,尝试使用flash里面的信息去 连接路由器
    {
      Serial.print(".");
      delay(1000);
      count++;
      if (count >= 5) {
        WIFI_Status = false;
        Serial.println("WiFi连接失败,请用手机进行配网");
      }
    } else {
      smartConfig();          // smartConfig技术配网
      WiFi.persistent(true);  // 保存WiFi
    }
  }
  // 串口打印连接成功的IP地址
  Serial.println("连接成功");
  Serial.print("IP:");
  Serial.println(WiFi.localIP());
}



// 手动判断刷新
void Judge(void) {
  if (ALL_state) BUILTIN_SWITCH.print("on");
  else BUILTIN_SWITCH.print("off");

  if (Bedroom_state) {
    Bedroom_Light.print("on");
    Bedroom_Light.color("#FDB0AE");
    Bedroom_Light.text("卧室灯开");
  } else {
    Bedroom_Light.print("off");
    Bedroom_Light.color("#3C3C3C");
    Bedroom_Light.text("卧室灯关");
  }
}


void setup() {
  // 初始化串口
  Serial.begin(115200);
  BLINKER_DEBUG.stream(Serial);
  BLINKER_DEBUG.debugAll();

  // 初始化有LED的IO
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(Bedroom, OUTPUT);

  // 默认关
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(Bedroom, HIGH);

  // WIFI初始化
  WIFI_Init();

  // 运行blinker
  Blinker.begin(auth, WiFi.SSID().c_str(), WiFi.psk().c_str());
  Blinker.attachSummary(summary);

  Bedroom_Light.attach(Bedroom_callback);
  BUILTIN_SWITCH.attach(switch_callback);
}

void loop() {
  Blinker.run();
  Judge();
}
