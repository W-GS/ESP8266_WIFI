# ESP8266

#### 介绍
ESP8266_WIFI

#### 软件架构
1.  Arduino开发环境，使用C语言
2.  取模软件：PCtoLCD2002完美版


#### 安装教程

1.  Arduino官网下载
2.  ESP8266离线包
3.  安装ESP8266库

#### 使用说明

1.  更改自己的WIFI名，WIFI密码
2.  注册心知天气
3.  更改自己的心知天气密钥

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_ESP8266 分支
3.  提交代码
4.  新建 Pull Request
5.  W工作室
6.  网络资源


#### 特技

1.  使用 Readme\_zh.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
